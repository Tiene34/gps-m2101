#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "fichier.h"
#include "traitement.h"

void main(){

	struct trame_data td;

	char *trame, *trame_copie;
	char *heure, *lat, *lon;

	trame = malloc(LG_TRAME+1);
	trame_copie = malloc(LG_TRAME+1);
	if (ecritureTrame()){
		if (recupererTrameVerifiee(trame)){

			printf("Le programme avance...\n");
			strncat(trame_copie,trame,LG_TRAME);
			extraire_champs(&td,trame);

			lat = convertLatLong(td.latitude,1);
			lon = convertLatLong(td.longitude,2);
			heure = formaterHeureReception(td.heure);

			affichage(trame_copie,heure,lat,lon);
		        free(lat);
        		free(lon);
			free(heure);
			printf("%s","C'est une réussite, allez voir dans donnees_extraites.txt\n");
		}

	}
	free(trame);
	free(trame_copie);
}

