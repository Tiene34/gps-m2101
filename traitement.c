#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "traitement.h"

int verifierTrame(char *txt){

	if (strncmp(txt,"$GPGGA,",7) == 0 && strlen(txt) > 14){
		return 1;
	}
	printf("Cette trame n'est pas du type GPGGA");
	return 0;

}

void extraire_champs(struct trame_data * ads,char *txt){

	const char del[2] = ",";
	char *p;
	int total = 0;

	p = strtok(txt,del);
	strncpy(ads->format,p,6);

	total += strlen(ads->format) + 1;

	p = strtok(txt+total,del);
	strncpy(ads->heure,p,11);

	total += strlen(ads->heure) + 1;

	p = strtok(txt+total, del);
	strncpy(ads->latitude,p,10);

	total += strlen(ads->latitude) + 1;

	p = strtok(txt+total, del);
        strncpy(ads->orientationLatitude,p,1);

	total += strlen(ads->orientationLatitude) + 1;

        p = strtok(txt+total, del);
        strncpy(ads->longitude,p,11);

        total += strlen(ads->longitude) + 1;

        p = strtok(txt+total, del);
        strncpy(ads->orientationLongitude,p,1);

}

char * convertLatLong(char * txt, int latLong){

	int r, z, c;
	double d,i;
	char *p;
	char *renvoi = NULL;
	char minutes[3];
	char secondes[5];

	renvoi = malloc(11);

	p = txt;

	r = latLong;
	z = 0;

	while (strncmp(txt,"0",1) == 0 && r>0){
		r--;
		z++;
	}

	p += z;
	strncat(renvoi,p,r+1);
	p -= z;

	p += latLong+1;

	d = atof(txt);
	i = 0;

	do{
		i++;
	}while (i*100/60 < d);
	i--;

	d -= i*5/3;
	d *= 3600;

	c=i;

	sprintf(minutes,"%d",c);
	strcat(renvoi,"°");
	strncat(renvoi,minutes,2);
	strcat(renvoi,"'");

	c = d;
	sprintf(secondes,"%d",c);
	strncat(renvoi,secondes,2);
	strcat(renvoi,".");

	p = secondes+2;
	strncat(renvoi,p,2);
	strcat(renvoi,"\"");

	return renvoi;
}

char * formaterHeureReception(char *txt){

	char *renvoi;
	char *p;

	renvoi = malloc(10);
	p = txt;
	strncat(renvoi,p,2);
	p += 2;
	strcat(renvoi,"h");
	strncat(renvoi,p,2);
	p += 2;
	strcat(renvoi,"m");
	strncat(renvoi,p,2);
	strcat(renvoi,"s");

	return renvoi;
}
