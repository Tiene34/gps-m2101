#include "fichier.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int ecritureTrame(void){
	FILE* ecriture;

	char *trame_choisie;
	trame_choisie = malloc(sizeof(char)*(LG_TRAME+1));

	strncpy(trame_choisie,"$GPGGA,064036.249,4836.5375,N,00740.9373,E,1,04,3.2,200.2,M,,,,0000*0E",LG_TRAME);

        ecriture = fopen("trame.txt","w");
        if (ecriture == NULL){
               	printf("Erreur à l'ouverture du fichier texte\n");
		return 0;
       	}
        fprintf(ecriture,"   Voici la trame de type GPGGA à étudier :\n\n");
	fprintf(ecriture,"%s\n\n\n",trame_choisie);
	fclose(ecriture);
	free(trame_choisie);

	return 1;
}

int recupererTrameVerifiee(char *ptr){

	FILE* recuperation;
	int trouve;

	recuperation = fopen("trame.txt","r");
	if (recuperation == NULL){
		printf("Erreur à l'ouverture du fichier texte\n");
		return 0;
	}
	trouve = 0;
	while(!trouve && !feof(recuperation)){
		fgets(ptr,LG_TRAME+1,recuperation);
		if ((strncmp(ptr,"$GPGGA,",7) == 0) && (strlen(ptr)>14)){
			trouve=1;
		}
	}
	if (!trouve){
		printf("\n  Aucune Trame de type GPGGA dans le fichier \n\n");
		return 0;
	}
	fclose(recuperation);
	return 1;
}

int affichage(char *trame, char *heure, char *lat, char *lon){

	FILE* affichage;

	affichage = fopen("donnees_extraites.txt","w");

	if (affichage == NULL){
		printf("\n   Erreur à l'ouverture du fichier donnees_extraites.txt\n");
		return 0;
	}

	fprintf(affichage,"\n\n          -----------------------------------------------------------------------\n");
	fprintf(affichage,"                         Recuperation des données de trames GPS\n");
	fprintf(affichage,"          -----------------------------------------------------------------------\n\n");
	fprintf(affichage,"         La trame de départ : \n    %s\n\n",trame);
	fprintf(affichage,"     Heure d'émission de la trame GPS : %s\n",heure);
	fprintf(affichage,"   Données exprimées en format DMS : \n");
	fprintf(affichage,"     Latitude : %s\n",lat);
	fprintf(affichage,"     Longitude : %s\n\n\n",lon);

	fclose(affichage);
	return 1;
}

