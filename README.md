# GPS M2101

Ce projet a été réalisé par Salim Zenini (groupe S2A) et Etienne Cotte (groupe S2E).

Le but de ce programme est d'extraire les champs d'une trame GPS, en écrivant celle-ci dans un fichier.
Ensuite, nous devions la récupérer, extraire les champs et convertir les latitudes et longitudes dans les bonnes unités.

Le fichier main.c appelé fichier.c composé des fonctions ci-dessous :
    - ecritreTrame renvoyant un int
    - recuperTrameVerifiee renvoyant un int et ayant un pointeur de char en paramètre
    - affichage ayant 4 pointeurs de char en paramètres
                        traitement.c composé des fonctions ci-dessous :
    - verifierTrame renvoyant un int et ayant un pointeur de char en paramètre
    - extraire_champs ayant un pointeur de structure et un pointeur de char en paramètres
    - convertLatLong renvoyant un pointeur de char et ayant un pointeur de char et un entier en paramètres
    - formaterHeureReception renvoyant un pointeur de char et ayant un pointeur de char en paramètre