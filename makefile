all : main

main : traitement.o fichier.o main.c
	gcc $^ -o $@

traitement.o : traitement.c traitement.h
	gcc -c $<

fichier.o : fichier.c fichier.h
	gcc -c $<

clean : 
	rm main
	rm *.o
	rm *.txt
